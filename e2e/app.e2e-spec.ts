import { CrearDigitalAndroidSetupPage } from './app.po';

describe('crear-digital-android-setup App', () => {
  let page: CrearDigitalAndroidSetupPage;

  beforeEach(() => {
    page = new CrearDigitalAndroidSetupPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
