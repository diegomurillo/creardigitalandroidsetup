const {ipcMain} = require('electron')
const storage = require("electron-json-storage");


module.exports.configSaveModule = function(app){
    ipcMain.on('save-config',function(event, key, value){
        storage.set(key, value, (error) => {
            if (error) event.returnValue = error; else event.returnValue = true;
        });
    });

    ipcMain.on('retrieve-config',function(event, key){
        storage.get(key, (error, data) => {
            console.log(`error = ${error} data = ${data}`)
            if (error) 
                event.returnValue = error 
            else {
                if(Object.keys(data).length === 0 && data.constructor === Object)
                    event.returnValue = null
                else
                    event.returnValue = data
            }
        });
    });

    ipcMain.on('remove-config',function(event, key){
        storage.remove(key, (error, data) => {
            console.log(`error = ${error} data = ${data}`)
            if (error) event.returnValue = error; else event.returnValue = true
        });
    });
}