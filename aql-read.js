const {ipcMain} = require('electron')
const fs = require('fs')

module.exports.aqlReadModule = function(app){

    ipcMain.on("aql-read-file", function(event,aqlFileName){
        event.returnValue = fs.readFileSync(app.getAppPath() + `/aql/${aqlFileName}.js`)
    })
}