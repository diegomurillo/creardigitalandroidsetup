const {
  app,
  BrowserWindow
} = require('electron')
const path = require('path');
const url = require('url');
const fs = require('fs');
const {configSaveModule} = require('./config-save')
const {aqlReadModule} = require('./aql-read')

let win = null;

configSaveModule(app);
aqlReadModule(app);

app.on('ready', function () {

  // Initialize the window to our specified dimensions
  win = new BrowserWindow({
    width: 1000,
    height: 600,
    'web-preferences': {'web-security': false}
  });



  // Specify entry point
  if (process.env.PROD == undefined || process.env.PROD != "true") {
    console.log("starting in DEV mode...")
    win.loadURL('http://localhost:4200');

    // Show dev tools
    // Remove this line before distributing
    //win.webContents.openDevTools();

  } else if (process.env.PROD == "true") {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  // Remove window once app is closed
  win.on('closed', function () {
    win = null;
  });

});

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});
