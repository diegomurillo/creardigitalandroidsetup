import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpModule} from '@angular/http'
import { ArtifactSearchService } from './artifact-search.service';
import { AqlReadService } from './aql-read.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [],
  providers: [ArtifactSearchService, AqlReadService]
})
export class ArtifactRestModule { }
