import { TestBed, inject } from '@angular/core/testing';

import { ArtifactFileService } from './artifact-search.service';

describe('ArtifactSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArtifactSearchService]
    });
  });

  it('should ...', inject([ArtifactSearchService], (service: ArtifactSearchService) => {
    expect(service).toBeTruthy();
  }));
});
