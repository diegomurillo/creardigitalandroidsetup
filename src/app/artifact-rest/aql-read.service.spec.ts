import { TestBed, inject } from '@angular/core/testing';

import { AqlReadServiceService } from './aql-read-service.service';

describe('AqlReadServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AqlReadServiceService]
    });
  });

  it('should ...', inject([AqlReadServiceService], (service: AqlReadServiceService) => {
    expect(service).toBeTruthy();
  }));
});
