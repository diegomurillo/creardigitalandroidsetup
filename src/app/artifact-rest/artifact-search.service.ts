import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { ArtifactoryAQLSearchResult } from '../crear-modules/modules-vos';
import { CrearConfigService, CrearConfigs } from '../crear-config/crear-config.service';
import { ArtifactoryConfig } from '../crear-config/config-vos';
import 'rxjs/add/observable/fromPromise'
import 'rxjs/add/operator/mergeMap'

@Injectable()
export class ArtifactSearchService {
  private aqlSearchAPI: string = "/api/search/aql"

  constructor(private http: Http, private crearConfig: CrearConfigService) { }

  /**
   * fileList
   */
  public aqlSearch(username: string, password: string, aqlSearch: string): Observable<ArtifactoryAQLSearchResult> {


    let configPromise = this.crearConfig.retrieveConfig<ArtifactoryConfig>(ArtifactoryConfig, CrearConfigs.ARTIFACTORY_CONFIG, new ArtifactoryConfig())
    return Observable.fromPromise(configPromise)
      .flatMap((artifactoryConfig) => {
        let headers = new Headers({ 'Content-Type': 'raw', 'Authorization': 'Basic' + btoa(`${username}:${password}`) });
        let options = new RequestOptions({ headers: headers, method: "post" });
        return this.http.post(artifactoryConfig.url + this.aqlSearchAPI, aqlSearch, options)
      })
      .map((response) => <ArtifactoryAQLSearchResult>response.json())
      .do(null, (error) => {
        console.error(error)
      })
  }
}
