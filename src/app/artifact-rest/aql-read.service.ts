import { Injectable } from '@angular/core';
import { ipcRenderer } from 'electron'

@Injectable()
export class AqlReadService {

  constructor() { }


  public readAql(aqlFileName:string): string{
    return ipcRenderer.sendSync('aql-read-file', aqlFileName) as any
  }
}
