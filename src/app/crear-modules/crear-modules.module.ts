import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesSelectComponent } from './modules-select/modules-select.component';
import { CrearConfigModule } from '../crear-config/crear-config.module';
import { ArtifactRestModule } from '../artifact-rest/artifact-rest.module';

@NgModule({
  imports: [
    CommonModule,
    ArtifactRestModule,
    CrearConfigModule
  ],
  declarations: [ModulesSelectComponent],
  exports: [ModulesSelectComponent]
})
export class CrearModulesModule { }
