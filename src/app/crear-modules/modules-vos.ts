

export class Module{
    constructor (private _companyGroup:string, private _artifact:string, private _version:string){

    }

    get companyGroup(){
        return this._companyGroup
    }

    set companyGroup(companyGroup:string){
        this._companyGroup = companyGroup
    }

    get artifact(){
        return this._artifact
    }

    set artifact(artifact:string){
        this._artifact = artifact
    }

    get version(){
        return this._version
    }

    set version(version:string){
        this._version = version
    }
}

export class ArtifactoryAQLSearchResultItem{
    repo:string
    path:string
    name:string
    type:string
}

export class ArtifactoryAQLSearchResult{
    results:Array<ArtifactoryAQLSearchResultItem>
}