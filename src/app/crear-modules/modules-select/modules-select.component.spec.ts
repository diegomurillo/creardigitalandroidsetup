import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesSelectComponent } from './modules-select.component';

describe('ModulesSelectComponent', () => {
  let component: ModulesSelectComponent;
  let fixture: ComponentFixture<ModulesSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulesSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
