import { Component, OnInit } from '@angular/core';
import { ArtifactSearchService } from '../../artifact-rest/artifact-search.service';
import { AqlReadService } from '../../artifact-rest/aql-read.service';
import { CrearConfigService, CrearConfigs } from '../../crear-config/crear-config.service';
import { ArtifactoryConfig } from '../../crear-config/config-vos';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap'
import { ArtifactoryAQLSearchResultItem, ArtifactoryAQLSearchResult, Module } from '../modules-vos';

@Component({
  selector: 'app-modules-select',
  templateUrl: './modules-select.component.html',
  styleUrls: ['./modules-select.component.css']
})
export class ModulesSelectComponent implements OnInit {

  constructor(private crearConfig: CrearConfigService,private artifactSearch:ArtifactSearchService, private aqlRead:AqlReadService) { }

  ngOnInit() {
    this.fetchModules()
  }

  private async fetchModules(){
    let artifactoryConfig = await this.crearConfig.retrieveConfig<ArtifactoryConfig>(ArtifactoryConfig,CrearConfigs.ARTIFACTORY_CONFIG, new ArtifactoryConfig())
    let aqlSearchByCompanyPackage =  this.aqlRead.readAql("search_by_company_package")

    this.artifactSearch
      .aqlSearch(artifactoryConfig.username, artifactoryConfig.password, aqlSearchByCompanyPackage)
        .flatMap((artifactoryResult, index) => {
          return Observable.of(artifactoryResult.results)
                  .map((item,index) => item[index])
        })
        .filter((artifactoryResultItem) => {
          return artifactoryResultItem.type == 'file' && artifactoryResultItem.name.endsWith('.arr')
        })
        .map((artifactoryResultItem) => {
          let version =  /-([0-9]\..*)\.aar/g.exec(artifactoryResultItem.name)[1]
          let artifact = /(.*)-[0-9]/g.exec(artifactoryResultItem.name)[1]

          console.log(version, artifact)
          return new Module(artifactoryConfig.companyPackage, artifact, version)
        }).subscribe()
  }
}
