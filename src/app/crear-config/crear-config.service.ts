import { Injectable } from '@angular/core';
import { ArtifactoryConfig } from './config-vos';
import { GlobalUtils } from '../globalutils.utils';
import { ipcRenderer } from 'electron'

declare var require: any


export const CrearConfigs = {
  ARTIFACTORY_CONFIG: 'ARTIFACTORY_CONFIG' as 'ARTIFACTORY_CONFIG'
}

@Injectable()
export class CrearConfigService {

  constructor() { }

  public saveConfig(key: keyof typeof CrearConfigs, value: Object): Promise<boolean> {

    return new Promise<boolean>((resolve, reject) => {
      ipcRenderer.sendSync('save-config', key, value)
    })

  }


  public retrieveConfig<T>(typeconst:{new(...args : any[]):T;}, key: keyof typeof CrearConfigs, defaultValue: T): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      
      let data:T = new typeconst(ipcRenderer.sendSync('retrieve-config', key))
      if (data != null && data != undefined)
        resolve(data)
      else
        resolve(defaultValue)
    })
  }

  public reset<T>(key: keyof typeof CrearConfigs, defaultValue: T): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      let removed = ipcRenderer.sendSync('remove-config', key)
      if (removed)
        resolve(defaultValue)
      else
        reject(removed)
    })
  }

}
