import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ArtifactoryConfig } from '../config-vos';
import { CrearConfigService, CrearConfigs } from '../crear-config.service';
import { MdInputDirective, MdSnackBar } from '@angular/material'
import * as Rx from 'rx';

@Component({
  selector: 'app-crear-settings',
  templateUrl: './crear-settings.component.html',
  styleUrls: ['./crear-settings.component.css']
})
export class CrearSettingsComponent implements OnInit, AfterViewInit {
  @Input()
  config: ArtifactoryConfig = new ArtifactoryConfig()
  @ViewChild('config.url') configUrlInput: ElementRef
  @ViewChild('config.companyPackage') configCompanyPackageInput: ElementRef
  updateTimeout:number = 1000

  constructor(private configService: CrearConfigService, private snackBar:MdSnackBar) {
  }

  async ngOnInit() {
    this.config = await this.configService.retrieveConfig<ArtifactoryConfig>(ArtifactoryConfig, CrearConfigs.ARTIFACTORY_CONFIG, new ArtifactoryConfig())
  }

  ngAfterViewInit() {
    this.setupFormUpdateEvents()
  }

  private setupFormUpdateEvents() {
    let configUrlObs$ = Rx.Observable.fromEvent(this.configUrlInput.nativeElement, 'keyup')
    let configCompanyPackObs$ = Rx.Observable.fromEvent(this.configCompanyPackageInput.nativeElement, 'keyup')

    var lastInterval = 0
    Rx.Observable.merge(configUrlObs$, configCompanyPackObs$)
      .debounce(this.updateTimeout)
      .subscribe(() => {
        console.log("artifactory configs saved")
        this.configService.saveConfig(CrearConfigs.ARTIFACTORY_CONFIG, this.config)
        this.snackBar.open("cambios guardados...", null, {
          duration: 700
        })
      })
  }

  async reset() {
    this.config = await this.configService.reset<ArtifactoryConfig>(CrearConfigs.ARTIFACTORY_CONFIG, new ArtifactoryConfig())
  }
}
