import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearSettingsComponent } from './crear-settings.component';

describe('CrearSettingsComponent', () => {
  let component: CrearSettingsComponent;
  let fixture: ComponentFixture<CrearSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
