import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CrearConfigService } from './crear-config.service';
import { CrearSettingsComponent } from './crear-settings/crear-settings.component';
import { MdInputModule, MdIconModule, MdButtonModule, MdCardModule, MdSnackBarModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MdInputModule,
    MdButtonModule,
    MdIconModule,
    MdCardModule,
    MdSnackBarModule
  ],
  declarations: [CrearSettingsComponent],
  exports: [CrearSettingsComponent],
  providers: [CrearConfigService]
})
export class CrearConfigModule { }
