import { TestBed, inject } from '@angular/core/testing';

import { CrearConfigService } from './crear-config.service';

describe('CrearConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrearConfigService]
    });
  });

  it('should ...', inject([CrearConfigService], (service: CrearConfigService) => {
    expect(service).toBeTruthy();
  }));
});
