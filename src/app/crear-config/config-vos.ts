export class ArtifactoryConfig{

    private _url:string = "http://localhost:8081/artifactory"
    private _username:string = "admin"
    private _password:string = "123456"
    private _companyPackage:string = "com.creardigital"

    constructor(obj:any = null){
        if(obj != null)
            Object.assign(this, obj)
    }

    get url(){
        return this._url
    }

    set url(url:string){
        this._url = url
    }

    get username(){
        return this._username
    }

    set username(username:string){
        this._username = username
    }

    get password(){
        return this._password
    }

    set password(password:string){
        this._password = password
    }

    get companyPackage(){
        return this._companyPackage
    }

    set companyPackage(packageName:string){
        this._companyPackage = packageName
    }
}