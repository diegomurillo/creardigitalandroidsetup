import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CrearModulesModule } from '../crear-modules/crear-modules.module';
import { CrearConfigModule } from '../crear-config/crear-config.module';
import { CrearSettingsComponent } from '../crear-config/crear-settings/crear-settings.component';

const appRoutes: Routes = [
  {
    path: 'modules-select',
    component: CrearModulesModule
  },
  {
    path: 'artifactory-config',
    component: CrearSettingsComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    CrearModulesModule,
    CrearConfigModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
