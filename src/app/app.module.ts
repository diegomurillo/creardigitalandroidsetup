import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {MdTabsModule, MdInputModule} from '@angular/material';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { CrearConfigModule } from './crear-config/crear-config.module';
import { ModulesSelectComponent } from './crear-modules/modules-select/modules-select.component';
import { CrearModulesModule } from './crear-modules/crear-modules.module';
import { ArtifactRestModule } from './artifact-rest/artifact-rest.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdTabsModule,
    MdInputModule,
    AppRoutingModule,
    //ArtifactRestModule,
    CrearConfigModule,
    CrearModulesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
