
export class GlobalUtils{

    public static isEmpty(obj:Object){
        return Object.keys(obj).length === 0
    }
}