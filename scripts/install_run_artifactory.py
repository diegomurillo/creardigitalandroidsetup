#!/usr/bin/env python
""" install artifactory image using docker """
import subprocess
import sys
from time import sleep

ARTIFACTORY_CONTAINER = "artifactory-oss"

def check_if_installed(command):
    """ check if a command is installed """
    process = subprocess.Popen(["which", command], stdout=subprocess.PIPE)
    (stdout, err) = process.communicate()
    if not stdout:
        return False
    elif err != None:
        print err
        return False
    else:
        return True

def run_command_handle_errors(command):
    """ runs a Popen subprocess command """
    """if err != None:
        sys.exit(err)
    else:
        return stdout"""
    while command.poll() is None:
        line = command.stdout.readline() # This blocks until it receives a newline.
        print line
    # When the subprocess terminates there might be unconsumed output
    # that still needs to be processed.
    sleep(0.3)
    print command.stdout.read()
    if command.stderr != None and command.stderr.readline():
        print command.stderr.read()
        return False
    return True

def install_artifactory():
    """ installs artifactory using Docker """
    if check_if_installed("docker"):
        get_image_process = subprocess.Popen(["docker",
                                              "pull",
                                              "docker.bintray.io/jfrog/artifactory-oss:latest"
                                             ], stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)

        setup_artifactory_image = subprocess.Popen(["docker",
                                                    "volume",
                                                    "create",
                                                    "--name",
                                                    "artifactory_data"
                                                   ], stdout=subprocess.PIPE,
                                                   stderr=subprocess.PIPE)

        check_if_artifactory_running = \
            subprocess.Popen(["docker", "inspect",
                              "-f", "{{.State.Running}}", ARTIFACTORY_CONTAINER],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        setup_artifactory_container =\
            subprocess.Popen(["docker",
                              "run",
                              "--name",
                              ARTIFACTORY_CONTAINER,
                              "-d", "-v",
                              "artifactory_data:/var/opt/jfrog/artifactory",
                              "-p", "8081:8081",
                              "docker.bintray.io/jfrog/artifactory-oss:latest"
                             ], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        start_artifactory_container =\
            subprocess.Popen(["docker", "start", ARTIFACTORY_CONTAINER],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        artifactor_installed = run_command_handle_errors(check_if_artifactory_running)
        if not artifactor_installed:
            run_command_handle_errors(get_image_process)
            run_command_handle_errors(setup_artifactory_image)
            run_command_handle_errors(setup_artifactory_container)
        else:
            run_command_handle_errors(start_artifactory_container)
    else:
        sys.exit("you must install docker in your system")

install_artifactory()
